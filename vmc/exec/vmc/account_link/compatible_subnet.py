__func_alias__ = {"list_": "list", "set_": "set"}


async def list_(hub, ctx, account_id: str, **kwargs):
    return await hub.tool.vmc.org.get(
        ctx, "account-link/compatible-subnets", linkedAccountId=account_id, **kwargs,
    )


async def set_(hub, ctx, **kwargs):
    return await hub.tool.vmc.org.get(ctx, "account-link/compatible-subnets", **kwargs)
