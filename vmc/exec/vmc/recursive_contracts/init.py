from typing import List, Dict, Any
from dict_tools import data


async def post_list(hub, ctx) -> List[Dict[str, Any]]:
    if isinstance(ctx.ret, List):
        if all(isinstance(x, Dict) for x in ctx.ret):
            # This is the ideal situation
            return [data.NamespaceDict(x) for x in ctx.ret]
        else:
            return [data.NamespaceDict(x.__dict__) for x in ctx.ret]
    elif isinstance(ctx.ret, Dict):
        hub.log.warning(f"{ctx.ref} returned a dict, not a list")
        return data.NamespaceDict(ctx.ret)

    return ctx.ret


async def call(hub, ctx):
    try:
        return await ctx.func(*ctx.args, **ctx.kwargs)
    except Exception as e:
        if hub.SUBPARSER == "exec":
            # If exec was called from the command line then quit here
            # States and tests might want to keep the connection open after a failure
            await hub.acct.init.close()
        raise


async def post_get(hub, ctx) -> Dict[str, Any]:
    ret = ctx.ret
    if isinstance(ret, List) and len(ret) == 1:
        ret = ret[0]
    if not isinstance(ret, Dict) and hasattr(ret, "__dict__"):
        # Force the return to be a dictionary if it can be
        ret = ret.__dict__

    if isinstance(ret, Dict):
        sorted_ret = data.NamespaceDict()
        for k in sorted(ret.keys()):
            if k.startswith("_"):
                continue
            sorted_ret[k] = ret[k]
        return sorted_ret
    else:
        return ret
