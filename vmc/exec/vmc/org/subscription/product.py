__func_alias__ = {"list_": "list"}


async def list_(hub, ctx):
    return await hub.tool.vmc.org.get(ctx, "subscriptions/products")
