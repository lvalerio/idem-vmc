__func_alias__ = {"set_": "set"}


async def set_(hub, ctx, locale: str):
    return await hub.tool.vmc.api.post(ctx, "locale", json=dict(VmcLocale=locale))
