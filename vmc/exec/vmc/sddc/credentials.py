# https://developer.vmware.com/docs/vmc/latest/sddcs/addons/

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, name: str, sddc_id: str, add_on_type: str, **kwargs):
    return await hub.tool.vmc.org.get(
        ctx, f"sddcs/{sddc_id}/addons/{add_on_type}/credentials/{name}", **kwargs
    )


async def list_(hub, ctx, sddc_id: str, add_on_type: str, **kwargs):
    return await hub.tool.vmc.org.get(
        ctx, f"sddcs/{sddc_id}/addons/{add_on_type}/credentials", **kwargs
    )


async def update(
    hub,
    ctx,
    name: str,
    sddc_id: str,
    add_on_type: str,
    username: str,
    password: str,
    **kwargs,
):
    return await hub.tool.vmc.org.put(
        ctx,
        f"sddcs/{sddc_id}/addons/{add_on_type}/credentials/{name}",
        data=dict(username=username, password=password, **kwargs),
    )


async def create(
    hub,
    ctx,
    name: str,
    sddc_id: str,
    add_on_type: str,
    username: str,
    password: str,
    **kwargs,
):
    return await hub.tool.vmc.org.post(
        ctx,
        f"sddcs/{sddc_id}/addons/{add_on_type}/credentials",
        data=dict(name=name, username=username, password=password, **kwargs),
    )
