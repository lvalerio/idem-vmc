__func_alias__ = {"list_": "list"}


async def list_(hub, ctx):
    return await hub.tool.vmc.org.get(ctx, "reservations")


async def get(hub, ctx, reservation_id: str):
    return await hub.tool.vmc.org.get(ctx, f"reservations/{reservation_id}/mw")


async def update(hub, ctx, reservation_id: str, day_of_week: str, hour_of_day: int):
    return await hub.tool.vmc.org.put(
        ctx,
        f"reservations/{reservation_id}/mw",
        data=dict(day_of_week=day_of_week.upper(), hour_of_day=hour_of_day),
    )
