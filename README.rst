========
IDEM-VMC
========

VMC Cloud Provider for Idem


DEVELOPMENT
===========

Clone the `idem-vmc` repository and install with pip.

.. code:: bash

    git clone git@gitlab.com:saltstack/pop/idem-vmc.git
    pip install -e idem_vmc

ACCT
====

After installation the VMC Idem Provider execution and state modules will be accessible to the pop `hub`.
In order to use them we need to set up our credentials.
Follow the `instructions <https://docs.vmware.com/en/VMware-Cloud-services/services/Using-VMware-Cloud-Services/GUID-E2A3B1C1-E9AD-4B00-A6B6-88D31FCDDF7C.html>`_ to generate an api token.

Create a new file called `credentials.yaml` and populate it with credentials.
The `default` profile will be used automatically by `idem` unless you specify one with `--acct-profile=profile_name` on the cli.

There are many ways aws providers/profiles can be stored. See `acct backends <https://gitlab.com/saltstack/pop/acct-backends>`_
for more information.

A profile needs to specify the api key it uses and can optionally specify the api url.

credentials.yaml

..  code:: sls

    csp.token:
      default:
        refresh_token: dmd23q3au8ljyajcvhz207of4ivsn9vjiaxzez223qeagdpe0voqiasknykv58jt
        default_sddc_id: my_sddc_id
        default_org_id: my_org


Now encrypt the credentials file and add the encryption key and encrypted file path to the ENVIRONMENT.

The `acct` command should be available as it is a requisite of `idem` and `idem-aws`.
Encrypt the the credential file.

.. code:: bash

    acct encrypt credentials.yaml

output::

    -A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI=

Add these to your environment:

.. code:: bash

    export ACCT_KEY="-A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI="
    export ACCT_FILE=$PWD/credentials.yaml.fernet


USAGE
=====
A profile can be specified for use with a specific state.
If no profile is specified, the profile called "default", if one exists, will be used:

.. code:: sls

    ensure_resource_exists:
      vmc.sddc.sddc.present:
        - acct_profile: my-staging-env
        - name: idem_vmc_dfw
        - kwarg1: val1

It can also be specified from the command line when executing states.

.. code:: bash

    idem state --acct-profile my-staging-env my_state.sls

It can also be specified from the command line when calling an exec module directly.

.. code:: bash

    idem exec --acct-profile my-staging-env vmc.sddc.sddc.list

=======
VMCTOOL
=======

PyVMC and the vmc-import-export tool have been ported to idem-vmc.  Their functionality has been implemented
as extensions of the `vmctool` command.
Their functionality will soon be completely transferred over to idem exec modules and state modules.


export
======

Export the entire vmc sddc configuration to json files.

.. code:: bash

    vmctool export all

You can also name specific functions in idem-vmc/vmc/vmc/export that you want to export.

.. code-block:: bash

    vmctool export sddc vpn ike-config ...

See available options for the export tool

.. code-block:: bash

    vmctool export --help

You are ready to use idem-vmc!!!
